﻿// -----------------------------------------------------------------------
// <copyright file="SocialMediaException.cs" company="NTB">
// NTB
// </copyright>
// -----------------------------------------------------------------------

namespace NTB.SocialMedia.Components.Exceptions
{
    using System;
    
    /// <summary>
    /// Creates the SocialMedia Exception
    /// </summary>
    internal class SocialMediaException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SocialMediaException"/> class.
        /// </summary>
        /// <remarks>Specifies an error message only.</remarks>
        /// <param name="message">The error message</param>
        public SocialMediaException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SocialMediaException"/> class.
        /// </summary>
        /// <remarks>
        /// Allows for specification of both an error message and an inner exception
        /// </remarks>
        /// <param name="message">The error message.</param>
        /// <param name="exception">Inner exception to wrap.</param>
        public SocialMediaException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }
}
