﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SocialMediaComponent.Designer.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The social media component.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SocialMedia.Components
{
    using NTB.SocialMedia.Components.Enums;

    /// <summary>
    /// The social media component.
    /// </summary>
    partial class SocialMediaComponent
    {
        /// <summary>
        /// The Enabled status for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="Enabled"/></remarks>
        protected bool enabled;

        /// <summary>
        /// The operation mode for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="OperationMode"/></remarks>
        protected OperationMode operationMode;

        /// <summary>
        /// The polling style for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="PollStyle"/></remarks>
        protected PollStyle pollStyle;

        /// <summary>
        /// The social media type.
        /// </summary>
        protected SocialMediaType socialMediaType;

        /// <summary>
        /// The maintanance mode for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="OperationMode"/></remarks>
        protected MaintenanceMode maintenanceMode;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// The poll timer.
        /// </summary>
        private System.Timers.Timer pollTimer;

        /// <summary>
        /// The files watcher.
        /// </summary>
        private System.IO.FileSystemWatcher filesWatcher;
        
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pollTimer = new System.Timers.Timer();
            this.filesWatcher = new System.IO.FileSystemWatcher();
            ((System.ComponentModel.ISupportInitialize)(this.pollTimer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filesWatcher)).BeginInit();
            // 
            // pollTimer
            // 
            this.pollTimer.Interval = 60000D;
            this.pollTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.pollTimer_Elapsed);
            // 
            // filesWatcher
            // 
            this.filesWatcher.EnableRaisingEvents = true;
            this.filesWatcher.Filter = "*.xml";
            this.filesWatcher.NotifyFilter = System.IO.NotifyFilters.FileName;
            this.filesWatcher.Created += new System.IO.FileSystemEventHandler(this.filesWatcher_Created);
            ((System.ComponentModel.ISupportInitialize)(this.pollTimer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filesWatcher)).EndInit();

        }

        #endregion    
    }
}
