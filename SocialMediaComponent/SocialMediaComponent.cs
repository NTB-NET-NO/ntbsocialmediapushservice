﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SocialMediaComponent.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SocialMedia.Components
{
    // Adding support for log4net
        // Adding support for component stuff
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Threading;
    using System.Xml;

    using log4net;

    using NTB.SocialMedia.Components.Classes;
    using NTB.SocialMedia.Components.Enums;
    using NTB.SocialMedia.Components.Exceptions;
    using NTB.SocialMedia.Components.Interfaces;

    /// <summary>
    /// The social media component.
    /// </summary>
    public partial class SocialMediaComponent : Component, IBaseSocialMedia
    {
        /// <summary>
        /// The Configured interval for this instance, used for Continous polling
        /// </summary>
        /// <remarks>
        ///   <para>Indicates the interval time in seconds for <c>Continous</c>polling. 60 means that the job runs every minute.</para>
        ///   <para>Default value: <c>60</c></para>
        /// </remarks>
        protected int Interval = 60;

        /// <summary>
        /// The Configured job name for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="InstanceName"/></remarks>
        protected string Name;

        /// <summary>
        /// Flag to indicate sucessful configure. Instance will not start polling nor handle messages if not properly Configured
        /// </summary>
        /// <remarks>Holds the Configured status of the instance. <c>True</c> means successfully Configured</remarks>
        protected bool Configured = false;

        /// <summary>
        /// Buffer size for the file watcher
        /// </summary>
        /// <remarks>
        ///   <para>Defines the internal buffer size the FileSystemWatcher when using this polling.
        /// Should not be set to high. The instance will fall back to contious polling on buffer overflows.</para>
        ///   <para>Default value: <c>4096</c></para>
        /// </remarks>
        protected int BufferSize = 4096;

        /// <summary>
        /// Input file filter
        /// </summary>
        /// <remarks>
        ///   <para>Defines what file types are being read from <see cref="FileInputFolder"/>.</para>
        ///   <para>Default value: <c>NTBIDType.None</c></para>
        /// </remarks>
        protected string FileFilter = "*.xml";

        /// <summary>
        /// Watch subdirs for new files as well
        /// </summary>
        /// <remarks>
        ///   <para>Defines if the instance should look in subdirectories within <see cref="FileInputFolder"/>.</para>
        ///   <para>Default value: <c>False</c></para>
        /// </remarks>
        protected bool IncludeSubdirs = false;

        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SocialMediaComponent));

        /// <summary>
        /// Busy status event
        /// </summary>
        private readonly AutoResetEvent _busyEvent = new AutoResetEvent(true);

        /// <summary>
        /// Exit control event
        /// </summary>
        private readonly AutoResetEvent _stopEvent = new AutoResetEvent(false);

        /// <summary>
        /// Initializes static members of the <see cref="SocialMediaComponent"/> class.
        /// </summary>
        static SocialMediaComponent()
        {
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SocialMediaComponent"/> class.
        /// </summary>
        public SocialMediaComponent()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SocialMediaComponent"/> class.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public SocialMediaComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        /// <summary>
        /// Gets a value indicating whether enabled.
        /// </summary>
        public bool Enabled
        {
            get
            {
                return enabled;
            }
        }

        /// <summary>
        /// Gets the instance name.
        /// </summary>
        public string InstanceName
        {
            get
            {
                return this.Name;
            }
        }

        /// <summary>
        /// Gets the operation mode.
        /// </summary>
        public OperationMode OperationMode
        {
            get
            {
                return OperationMode.Push;
            }
        }

        /// <summary>
        /// Gets the poll style.
        /// </summary>
        public PollStyle PollStyle
        {
            get
            {
                return pollStyle;
            }
        }

        /// <summary>
        /// Gets or sets the poll style.
        /// </summary>
        public SocialMediaType SocialMediaType { get; set; }

        /// <summary>
        /// Gets or sets the component state.
        /// </summary>
        public ComponentState ComponentState { get; set; }

        /// <summary>
        /// Gets or sets the poll delay.
        /// </summary>
        protected int PollDelay { get; set; }

        /// <summary>
        /// Gets or sets the file input folder.
        /// </summary>
        protected string FileInputFolder { get; set; }

        /// <summary>
        /// Gets or sets the file done folder.
        /// </summary>
        protected string FileDoneFolder { get; set; }

        /// <summary>
        /// Gets or sets the file error folder.
        /// </summary>
        protected string FileErrorFolder { get; set; }

        /// <summary>
        /// Gets or sets the message length.
        /// </summary>
        protected int MessageLength { get; set; }

        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        protected string Username { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        protected string Password { get; set; }

        /// <summary>
        /// Gets or sets the consumer key.
        /// </summary>
        protected string ConsumerKey { get; set; }

        /// <summary>
        /// Gets or sets the consumer secret.
        /// </summary>
        protected string ConsumerSecret { get; set; }

        /// <summary>
        /// Gets or sets the access token.
        /// </summary>
        protected string AccessToken { get; set; }

        /// <summary>
        /// Gets or sets the access token secret.
        /// </summary>
        protected string AccessTokenSecret { get; set; }

        /// <summary>
        /// Gets or sets the request token url.
        /// </summary>
        protected string RequestTokenUrl { get; set; }

        /// <summary>
        /// Gets or sets the authorize url.
        /// </summary>
        protected string AuthorizeUrl { get; set; }

        /// <summary>
        /// Gets or sets the access token url.
        /// </summary>
        protected string AccessTokenUrl { get; set; }

        /// <summary>
        /// Gets or sets the url target.
        /// </summary>
        protected string UrlTarget { get; set; }

        /// <summary>
        /// Gets or sets the x path expression.
        /// </summary>
        protected string XPathExpression { get; set; }

        /// <summary>
        /// The configure.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        public void Configure(XmlNode configNode)
        {
            // Basic configuration sanity check
            if (configNode.Attributes != null
                && (configNode == null || configNode.Name != "SocialMediaComponent"
                    || configNode.Attributes.GetNamedItem("name") == null))
            {
                throw new ArgumentException("The XML configuration node passed is invalid", "configNode");
            }

            if (Thread.CurrentThread.Name == null && configNode.Attributes != null)
            {
                Thread.CurrentThread.Name = configNode.Attributes.GetNamedItem("name").ToString();
            }

            // Getting the values for the Component instance
            try
            {
                if (configNode.Attributes != null)
                {
                    this.Name = configNode.Attributes["name"].Value;
                    this.enabled = Convert.ToBoolean(configNode.Attributes["enabled"].Value);
                    this.BufferSize = Convert.ToInt32(configNode.Attributes["buffersize"].Value);
                    this.FileFilter = configNode.Attributes["filefilter"].Value;
                    this.MessageLength = Convert.ToInt32(configNode.Attributes["messagelength"].Value);
                }

                ThreadContext.Stacks["NDC"].Push(InstanceName);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to configure this job instance!", ex);
            }

            // Basic operation
            try
            {
                if (configNode.Attributes != null)
                {
                    this.operationMode =
                        (OperationMode)
                        Enum.Parse(typeof(OperationMode), configNode.Attributes["operationmode"].Value, true);
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing OperationMode values in XML configuration node", ex);
            }

            try
            {
                if (configNode.Attributes != null)
                {
                    this.pollStyle =
                        (PollStyle)Enum.Parse(typeof(PollStyle), configNode.Attributes["pollstyle"].Value, true);
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing PollStyle values in XML configuration node", ex);
            }

            // Setting PollDelay to default three seconds
            this.PollDelay = 3;
            try
            {
                if (configNode.Attributes != null)
                {
                    this.PollDelay = Convert.ToInt32(configNode.Attributes["delay"].Value);
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing PollStyle values in XML configuration node", ex);
            }

            try
            {
                if (configNode.Attributes != null)
                {
                    this.socialMediaType =
                        (SocialMediaType)
                        Enum.Parse(typeof(SocialMediaType), configNode.Attributes["socialmediatype"].Value, true);
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing OperationMode values in XML configuration node", ex);
            }

            try
            {
                // Find the file folder to work with
                XmlNode folderNode = configNode.SelectSingleNode("Folders/FileInputFolder");

                // configNode.SelectSingleNode("Folder").Attributes("XMLOutputFolder");
                if (folderNode != null)
                {
                    FileInputFolder = folderNode.InnerText;

                    // Check if folder exists
                    DirectoryInfo di = new DirectoryInfo(FileInputFolder);
                    if (!di.Exists)
                    {
                        di.Create();
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Fatal("Not possible to get input-folder", exception);
            }

            // Find the file folder to work with
            try
            {
                XmlNode folderNode = configNode.SelectSingleNode("Folders/FileErrorFolder");

                if (folderNode != null)
                {
                    FileErrorFolder = folderNode.InnerText;

                    // Check if folder exists
                    DirectoryInfo di = new DirectoryInfo(FileErrorFolder);
                    if (!di.Exists)
                    {
                        di.Create();
                    }
                }

                /*
                Logger.InfoFormat(
                    "Converter job - Polling: {0} / File input folder: {1} / Enabled: {2}",
                    Enum.GetName(typeof(PollStyle), pollStyle),
                    FileInputFolder,
                    enabled);
                 */
            }
            catch (Exception exception)
            {
                Logger.Fatal("Not possible to get error-folder", exception);
            }

            // Find the file folder to work with
            try
            {
                XmlNode folderNode = configNode.SelectSingleNode("Folders/FileDoneFolder");

                if (folderNode != null)
                {
                    FileDoneFolder = folderNode.InnerText;

                    // Check if folder exists
                    DirectoryInfo di = new DirectoryInfo(FileDoneFolder);
                    if (!di.Exists)
                    {
                        di.Create();
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Fatal("Not possible to get done-folder", exception);
            }

            #region credentials

            try
            {
                XmlNode credentialNode = configNode.SelectSingleNode("Credentials");

                if (credentialNode != null)
                {
                    if (credentialNode.Attributes["username"] != null)
                    {
                        this.Username = credentialNode.Attributes["username"].Value;
                    }

                    if (credentialNode.Attributes["password"] != null)
                    {
                        this.Password = credentialNode.Attributes["password"].Value;
                    }

                    if (credentialNode.Attributes["consumerkey"] != null)
                    {
                        this.ConsumerKey = credentialNode.Attributes["consumerkey"].Value;
                    }

                    if (credentialNode.Attributes["consumersecret"] != null)
                    {
                        this.ConsumerSecret = credentialNode.Attributes["consumersecret"].Value;
                    }

                    if (credentialNode.Attributes["accesstoken"] != null)
                    {
                        this.AccessToken = credentialNode.Attributes["accesstoken"].Value;
                    }

                    if (credentialNode.Attributes["accesstokensecret"] != null)
                    {
                        this.AccessTokenSecret = credentialNode.Attributes["accesstokensecret"].Value;
                    }

                    if (credentialNode.Attributes["requesttokenurl"] != null)
                    {
                        this.RequestTokenUrl = credentialNode.Attributes["requesttokenurl"].Value;
                    }

                    if (credentialNode.Attributes["authorizeurl"] != null)
                    {
                        this.AuthorizeUrl = credentialNode.Attributes["authorizeurl"].Value;
                    }

                    if (credentialNode.Attributes["accesstokenurl"] != null)
                    {
                        this.AccessTokenUrl = credentialNode.Attributes["accesstokenurl"].Value;
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Fatal("Not possible to get credentials", exception);
            }

            #endregion credentials

            #region Url

            try
            {
                XmlNode urlNode = configNode.SelectSingleNode("Url");

                if (urlNode != null)
                {
                    UrlTarget = urlNode.InnerText;
                }
            }
            catch (Exception exception)
            {
                Logger.Fatal("Not possible to get Url", exception);
            }

            try
            {
                XmlNode xpathNode = configNode.SelectSingleNode("Xpath");

                if (xpathNode != null)
                {
                    XPathExpression = xpathNode.InnerText;
                }
            }
            catch (Exception exception)
            {
                Logger.Fatal("Not possible to get Url", exception);
            }

            #endregion Url

            #region Set up polling

            try
            {
                // Switch on pollstyle
                switch (pollStyle)
                {
                    case PollStyle.Continous:
                        Interval = Convert.ToInt32(configNode.Attributes["Interval"].Value);
                        if (Interval == 0)
                        {
                            Interval = 5000;
                        }

                        pollTimer.Interval = Interval * 1000; // short startup - interval * 1000;
                        Logger.DebugFormat("Poll interval: {0} seconds", Interval);

                        break;

                    case PollStyle.Scheduled:
                        throw new NotSupportedException("Invalid polling style for this job type");

                    case PollStyle.FileSystemWatch:

                        // Check for config overrides
                        if (configNode.Attributes.GetNamedItem("BufferSize") != null)
                        {
                            BufferSize = Convert.ToInt32(configNode.Attributes["BufferSize"].Value);
                        }

                        Logger.DebugFormat("FileSystemWatcher buffer size: {0}", BufferSize);

                        // Set values
                        filesWatcher.Path = FileInputFolder;
                        filesWatcher.Filter = FileFilter;
                        filesWatcher.IncludeSubdirectories = IncludeSubdirs;
                        filesWatcher.InternalBufferSize = BufferSize;

                        // Do not start the event watcher here, wait until after the startup cleaning job
                        pollTimer.Interval = 5000; // short startup;

                        break;

                    default:

                        // Unsupported pollstyle for this object
                        throw new NotSupportedException("Invalid polling style for this job type");
                }

                Configured = true;
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                Configured = false;
            }

            #endregion
        }

        /// <summary>
        /// The start.
        /// </summary>
        public void Start()
        {
            if (!Configured)
            {
                throw new SocialMediaException(
                    "SocialMediaComponent is not properly Configured. SocialMediaComponent::Start() Aborted!");
            }

            if (!Enabled)
            {
                throw new SocialMediaException(
                    "SocialMediaComponent is not Enabled. SocialMediaComponent::Start() Aborted!");
            }

            Logger.Debug("In Start() " + InstanceName + ".");

            _stopEvent.Reset();
            _busyEvent.Set();

            ComponentState = ComponentState.Running;

            pollTimer.Start();
        }

        /// <summary>
        /// The stop.
        /// </summary>
        public void Stop()
        {
            if (!Configured)
            {
                throw new SocialMediaException(
                    "SocialMediaComponent is not properly Configured. SocialMediaComponent::Stop() Aborted");
            }

            if (!Enabled)
            {
                throw new SocialMediaException(
                    "SocialMediaComponent is not properly Configured. SocialMediaComponent::Stop() Aborted");
            }

            // Signal Stop
            _stopEvent.Set();

            // Stop events
            filesWatcher.EnableRaisingEvents = false;

            if (!_busyEvent.WaitOne(30000))
            {
                Logger.InfoFormat(
                    "Instance did not complete properly. Data may be inconsistent. Job: {0}", InstanceName);
            }

            ComponentState = ComponentState.Halted;

            // Kill polling
            pollTimer.Stop();
        }

        /// <summary>
        /// The files watcher_ created.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void filesWatcher_Created(object sender, FileSystemEventArgs e)
        {
            ThreadContext.Properties["JOBNAME"] = InstanceName;
            Logger.DebugFormat("SocialMediaComponent::filesWatcher_Created() hit with file data: {0}", e.FullPath);

            if (PollDelay > 0)
            {
                // We take the value and multiply with thousand - to get the seconds... 
                int pollTime = PollDelay * 1000;

                Logger.Debug(
                    "We are waiting: " + pollTime + " Seconds before starting to push message to social media!");

                Thread.Sleep(pollTime);

                try
                {
                    // Wait for the busy state
                    _busyEvent.WaitOne();

                    // Getting the list of files we are to push
                    List<string> files = new List<string> { e.FullPath };
                    switch (socialMediaType)
                    {
                        case SocialMediaType.Twitter:
                            TwitterPost twitterPost = new TwitterPost
                                {
                                    MessageLength = this.MessageLength,
                                    Files = files, 
                                    FileDoneFolder = FileDoneFolder,
                                    UrlTarget = UrlTarget,
                                    XpathExpression = XPathExpression,
                                    AccessToken = AccessToken,
                                    AccessTokenSecret = AccessTokenSecret,
                                    AccessTokenUrl = AccessTokenUrl,
                                    ConsumerKey = ConsumerKey,
                                    ConsumerSecret = ConsumerSecret,
                                    
                                    FileErrorFolder = this.FileErrorFolder
                                };

                            twitterPost.PostMessage();

                            _busyEvent.Set();
                            break;

                        case SocialMediaType.Facebook:

                            break;

                        case SocialMediaType.GooglePlus:

                            break;
                    }
                }
                catch (Exception exception)
                {
                    Logger.Debug("Something happened while doing filesWatcher_Created!");
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
            }
        }

        /// <summary>
        /// The poll timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void pollTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
             ThreadContext.Properties["JOBNAME"] = InstanceName;

            Logger.Debug("MaintenanceComponent::pollTimer_Elapsed() hit");
            Logger.Debug("We have the following settings: Interval: " + pollTimer.Interval);

            ThreadContext.Properties["JOBNAME"] = InstanceName;
            Logger.DebugFormat("MaintenanceComponent::pollTimer_Elapsed() hit");

            // Reset flags and pollers
            _busyEvent.WaitOne();
            pollTimer.Stop();

            try
            {
                Logger.Debug("InputFolder for " + InstanceName + " : ");

                // Process any waiting files
                Logger.Info("Process any waiting files");
                List<string> files = new List<string>(Directory.GetFiles(FileInputFolder, "*.xml", SearchOption.TopDirectoryOnly));

                // Getting the list of files we are to push
                switch (socialMediaType)
                {
                    case SocialMediaType.Twitter:
                        TwitterPost twitterPost = new TwitterPost
                        {
                            Files = files,
                            FileDoneFolder = FileDoneFolder,
                            FileErrorFolder = FileErrorFolder,
                            UrlTarget = UrlTarget,
                            XpathExpression = XPathExpression,
                            AccessToken = AccessToken,
                            AccessTokenSecret = AccessTokenSecret,
                            AccessTokenUrl = AccessTokenUrl,
                            ConsumerKey = ConsumerKey,
                            ConsumerSecret = ConsumerSecret,
                            MessageLength = this.MessageLength
                        };

                        twitterPost.PostMessage();

                        _busyEvent.Set();
                        break;

                    case SocialMediaType.Facebook:

                        break;

                    case SocialMediaType.GooglePlus:

                        break;
                }
                    
                // Enable the event listening
                if (pollStyle == PollStyle.FileSystemWatch)
                {
                    Logger.Debug("Setting filesWatcher.EnableRaisingEvents to true");
                    filesWatcher.EnableRaisingEvents = true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("SocialMediaComponent::pollTimer_Elapsed() error: " + ex.Message, ex);
            }
        }
    }
}