﻿// -----------------------------------------------------------------------
// <copyright file="IBaseSocialMedia.cs" company="NTB">
// NTB
// </copyright>
// -----------------------------------------------------------------------

namespace NTB.SocialMedia.Components.Interfaces
{
    using System.Xml;

    using NTB.SocialMedia.Components.Enums;

    /// <summary>
    /// The interface used by all components 
    /// </summary>
    public interface IBaseSocialMedia
    {
        /// <summary>
        /// Gets a value indicating whether the Enabled status of the instance.
        /// </summary>
        /// <value>The Enabled status.</value>
        bool Enabled { get; }

        /// <summary>
        /// Gets the name of the instance.
        /// </summary>
        /// <value>The name of the instance.</value>
        string InstanceName { get; }

        /// <summary>
        /// Gets the operation mode.
        /// </summary>
        /// <value>The operation mode.</value>
        OperationMode OperationMode { get; }

        /// <summary>
        /// Gets the poll style.
        /// </summary>
        /// <value>The poll style.</value>
        PollStyle PollStyle { get; }

        /// <summary>
        /// Gets the ComponentState
        /// </summary>
        ComponentState ComponentState { get; }
        
        /// <summary>
        /// Configure the component instance with all necessary data
        /// </summary>
        /// <param name="configNode">
        /// XmlNode that contains the configuration information
        /// </param>
        void Configure(XmlNode configNode);

        /// <summary>
        /// Starts the polling for this instance
        /// </summary>
        void Start();

        /// <summary>
        /// Stops the polling for this instance
        /// </summary>
        void Stop();
    }
}
