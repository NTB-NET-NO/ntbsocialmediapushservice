﻿namespace NTB.SocialMedia.Components
{
    partial class MainServiceComponent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.configFileWatcher = new System.IO.FileSystemWatcher();
            this.maintenanceTimer = new System.Timers.Timer();
            ((System.ComponentModel.ISupportInitialize)(this.configFileWatcher)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maintenanceTimer)).BeginInit();
            // 
            // configFileWatcher
            // 
            this.configFileWatcher.EnableRaisingEvents = true;
            this.configFileWatcher.Changed += new System.IO.FileSystemEventHandler(this.configFileWatcher_Changed);
            // 
            // maintenanceTimer
            // 
            this.maintenanceTimer.Enabled = true;
            this.maintenanceTimer.Interval = 60000D;
            this.maintenanceTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.maintenanceTimer_Elapsed);
            ((System.ComponentModel.ISupportInitialize)(this.configFileWatcher)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maintenanceTimer)).EndInit();

        }

        #endregion

        private System.IO.FileSystemWatcher configFileWatcher;
        private System.Timers.Timer maintenanceTimer;
    }
}
