namespace NTB.SocialMedia.Components.Enums
{
    /// <summary>
    /// Used to tell if the service component is running or has signaled that everything has to stop
    /// </summary>
    public enum ServiceMode
    {
        /// <summary>
        /// Running mode is when everything is OK
        /// </summary>
        Running,

        /// <summary>
        /// If the program is signaling halting, the service shall stop. 
        /// </summary>
        Halting

    }
}