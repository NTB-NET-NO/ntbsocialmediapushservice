namespace NTB.SocialMedia.Components.Enums
{
    /// <summary>
    /// Specifies the different days and which day is the first one. This so that we can support 
    /// scheduling on both day AND time
    /// </summary>
    

    public enum ScheduleType
    {
        /// <summary>
        /// A continously is a pollstyle that happens continously
        /// </summary>
        Continous,
        /// <summary>
        /// A daily ScheduleType is a pollstyle that happens every day
        /// </summary>
        Daily,

        /// <summary>
        /// A daily ScheduleType is a pollstyle that happens every week
        /// </summary>
        Weekly,

        /// <summary>
        /// A daily ScheduleType is a pollstyle that happens every month
        /// </summary>
        Monthly,

        /// <summary>
        /// A daily ScheduleType is a pollstyle that happens every year
        /// </summary>
        Yearly
    }


}