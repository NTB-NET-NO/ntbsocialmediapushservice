﻿
namespace NTB.SocialMedia.Components.Enums
{
    public enum MaintenanceMode
    {
        /// <summary>
        /// CheckMatches mode means that we are checking for matches that has caused us problems earlier in the day
        /// </summary>
        CheckMatches,

        /// <summary>
        /// CheckAllMatches mode means that we are checking for matches that has caused us problems in the past
        /// </summary>
        CheckAllMatches
    }
}
