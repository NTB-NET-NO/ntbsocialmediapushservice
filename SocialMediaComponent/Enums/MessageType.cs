﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageType.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Specifies the different days and which day is the first one. This so that we can support
//   scheduling on both day AND time
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SocialMedia.Components.Enums
{
    /// <summary>
    /// Specifies the different days and which day is the first one. This so that we can support 
    /// scheduling on both day AND time
    /// </summary>
    public enum MessageType
    {
        /// <summary>
        /// Message is of type Matchfact which means that we have name of scorers and such. Could also have name of squad
        /// </summary>
        MatchFacts,

        /// <summary>
        /// Message is of type Sport result which means that we have the name of the teams and the scores. We also have standings
        /// </summary>
        SportResult,

        /// <summary>
        /// Message of type Sport Message means that we have the name of the teams and the scores, but not standings
        /// </summary>
        SportMessage,

        /// <summary>
        /// Message of type Sport Schedule means that we only have the name of the teams. These are matches that happens in the future
        /// </summary>
        SportSchedule
    }
}