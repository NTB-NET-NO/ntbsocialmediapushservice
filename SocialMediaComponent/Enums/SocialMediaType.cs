﻿// -----------------------------------------------------------------------
// <copyright file="SocialMediaType.cs" company="NTB">
// NTB
// </copyright>
// -----------------------------------------------------------------------

namespace NTB.SocialMedia.Components.Enums
{
    /// <summary>
    /// SocialMedia Type is used to tell which type of social media we are to push the message to
    /// </summary>
    public enum SocialMediaType
    {
        /// <summary>
        /// SocialMedia is of type Twitter which means that we are to tweet the message
        /// </summary>
        Twitter,

        /// <summary>
        /// SocialMedia is of type Facebook which means that we are to update the facebook page for this "customer"
        /// </summary>
        Facebook,

        /// <summary>
        /// SocialMedia is of type Google+ which means that we are to update the google+ page for this "customer"
        /// </summary>
        GooglePlus
    }
}
