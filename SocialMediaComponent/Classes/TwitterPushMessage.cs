﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TwitterPushMessage.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SocialMedia.Components.Classes
{
    using System;
    using System.Linq;

    using LinqToTwitter;

    using log4net;

    /// <summary>
    /// Class to create twitter messages and push them to the clients twitter feed
    /// </summary>
    public class TwitterPushMessage
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TwitterPushMessage));

        /// <summary>
        /// The authorizer.
        /// </summary>
        private SingleUserAuthorizer authorizer;

        /// <summary>
        /// The twitter message.
        /// </summary>
        private string twitterMessage;

        /// <summary>
        /// Initializes a new instance of the <see cref="TwitterPushMessage"/> class.
        /// </summary>
        public TwitterPushMessage()
        {
            Logger.Info("Creating the TwitterPushMessage object, and connecting to Twitter");
        }

        /// <summary>
        /// Sets the message.
        /// </summary>
        public TwitterMessage SetMessage
        {
            set
            {
                twitterMessage = value.Publisher + @": " + value.Message + @" " + value.Url;
                if (twitterMessage.Length > MessageLength)
                {
                    string newMessage = value.Publisher + @": " + value.Message + @" " + value.Url;
                    string message = value.Message;

                    while (newMessage.Length > MessageLength)
                    {
                        string[] parts = message.Split(' ');
                        int partCount = parts.Count();
                        int partcheck = partCount - 1;
                        string partMessage = string.Empty; 
                        for (int i = 0; i < partcheck; i++)
                        {
                            partMessage += parts[i] + " ";
                        }
                        partMessage = partMessage.Trim();
                        newMessage = value.Publisher + @": " + partMessage + @" ... " + value.Url; // +value.Url;
                        message = string.Join(" ", partMessage);

                        Logger.Debug("Message is " + newMessage.Count() + " characters long...");
                    }

                    twitterMessage = newMessage;
                }
            }
        }

        /// <summary>
        /// Gets or sets the access token.
        /// </summary>
        public string OAuthToken { get; set; }

        /// <summary>
        /// Gets or sets the access token secret.
        /// </summary>
        public string AccessTokenSecret { get; set; }

        /// <summary>
        /// Gets or sets the access token url.
        /// </summary>
        public string AccessTokenUrl { get; set; }

        /// <summary>
        /// Gets or sets the consumer key.
        /// </summary>
        public string ConsumerKey { get; set; }

        /// <summary>
        /// Gets or sets the consumer secret.
        /// </summary>
        public string ConsumerSecret { get; set; }

        /// <summary>
        /// Gets or sets the message length
        /// </summary>
        public int MessageLength { get; set; }

        /// <summary>
        /// The authorize.
        /// </summary>
        public void Authorize()
        {
            authorizer = new SingleUserAuthorizer
                             {
                                 Credentials =
                                     new InMemoryCredentials
                                         {
                                             ConsumerKey = ConsumerKey,
                                             ConsumerSecret = ConsumerSecret,
                                             OAuthToken = this.OAuthToken,
                                             AccessToken = AccessTokenSecret
                                         }
                             };
            
        }
        
        /// <summary>
        /// The tweet.
        /// </summary>
        public void Tweet()
        {
            Logger.Info("Tweeting: " + twitterMessage);
            using (var twitterContext = new TwitterContext(authorizer))
            {
                var updateStatus = twitterContext.UpdateStatus(twitterMessage);
                
                Logger.Info("Status returned: " + updateStatus.StatusID + " for " + updateStatus.User.Name + ": " + updateStatus.Text);
            }
        }
    }
}
