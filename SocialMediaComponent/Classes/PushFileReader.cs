﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PushFileReader.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SocialMedia.Components.Classes
{
    using System;
    using System.IO;
    using System.Xml;

    /// <summary>
    /// Class used to read data from the article
    /// </summary>
    public class PushFileReader
    {
        /// <summary>
        /// Gets or sets the name of the file we are to read from
        /// </summary>
        public string Filename { get; set; }

        /// <summary>
        /// Gets or sets the content of the XML File we have read
        /// </summary>
        public XmlDocument XmlContent { get; set; }

        /// <summary>
        /// Gets or sets the xpath expression.
        /// </summary>
        public string XpathExpression { get; set; }
        
        /// <summary>
        /// The read file.
        /// </summary>
        /// <exception cref="Exception">
        /// Throws an exception if the file does not exist
        /// </exception>
        public void Read()
        {
            FileInfo fileInfo = new FileInfo(Filename);
            if (!fileInfo.Exists)
            {
                throw new Exception("File does not exist! Cannot continue!");
            }

            XmlContent = new XmlDocument();
            XmlContent.Load(Filename);
        }

        /// <summary>
        /// The get title.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetTitle()
        {
            XmlNode selectSingleNode = this.XmlContent.SelectSingleNode("/nitf/head/title");
            
            return selectSingleNode != null ? selectSingleNode.InnerText : string.Empty;
        }

        /// <summary>
        /// Gets the intro text for the article
        /// </summary>
        /// <returns>String containing the article intro</returns>
        public string GetIntro()
        {
            XmlNode selectSingleNode = this.XmlContent.SelectSingleNode("/nitf/body/body.content/p[@lede='true']");

            return selectSingleNode != null ? selectSingleNode.Value : string.Empty;
        }

        /// <summary>
        /// The get publisher.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetPublisher()
        {
            XmlNode selectSingleNode = this.XmlContent.SelectSingleNode("/nitf/head/meta[@name='NTBKilde']/@content");
            return selectSingleNode != null ? selectSingleNode.Value : string.Empty;
        }

        /// <summary>
        /// The get message id.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetMessageId()
        {
            XmlNode selectSingleNode = null;

            if (XpathExpression == null)
            {
                selectSingleNode = this.XmlContent.SelectSingleNode("/nitf/head/meta[@name='NTBStikkord']/@content");
            }
            else
            {
                selectSingleNode = this.XmlContent.SelectSingleNode(XpathExpression);
            }
            
            return selectSingleNode != null ? selectSingleNode.Value.Replace("PRM-NTB-", string.Empty) : string.Empty;
        }

        /// <summary>
        /// The get url.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetUrl()
        {
            return string.Empty;
        }
    }
}
