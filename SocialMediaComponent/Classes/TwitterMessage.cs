﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TwitterMessage.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SocialMedia.Components.Classes
{
    /// <summary>
    /// The twitter message.
    /// </summary>
    public class TwitterMessage
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the publisher.
        /// </summary>
        public string Publisher { get; set; }

        /// <summary>
        /// Gets or sets the url for the message
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the message length.
        /// </summary>
        public int MessageLength { get; set; }

        /// <summary>
        /// The generate message.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/> string containing the message.
        /// </returns>
        public string GenerateMessage()
        {
            string returnMessage = Publisher + ": " + Message + " - " + Url;

            int currentLength = returnMessage.Length;
            if (currentLength > MessageLength)
            {
                int diff = currentLength - MessageLength;
                return Publisher + ": " + Message.Substring(1, diff) + " - " + Url;
            }
            return string.Empty;
        }
    }
}