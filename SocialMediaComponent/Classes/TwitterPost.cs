﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TwitterPost.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SocialMedia.Components.Classes
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;

    using log4net;

    /// <summary>
    /// The class that does all the posting
    /// </summary>
    public class TwitterPost
    {
        /// <summary>
        /// The files.
        /// </summary>
        public List<string> Files = new List<string>();

        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TwitterPost));

        /// <summary>
        /// Initializes a new instance of the <see cref="TwitterPost"/> class.
        /// </summary>
        public TwitterPost()
        {
            Logger.Info("Initialized TwitterPost object");
        }

        /// <summary>
        /// Gets or sets the file message length.
        /// </summary>
        public int MessageLength { get; set; }

        /// <summary>
        /// Gets or sets the file done folder.
        /// </summary>
        public string FileDoneFolder { get; set; }

        /// <summary>
        /// Gets or sets the access token.
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// Gets or sets the access token secret.
        /// </summary>
        public string AccessTokenSecret { get; set; }

        /// <summary>
        /// Gets or sets the access token url.
        /// </summary>
        public string AccessTokenUrl { get; set; }

        /// <summary>
        /// Gets or sets the consumer key.
        /// </summary>
        public string ConsumerKey { get; set; }

        /// <summary>
        /// Gets or sets the consumer secret.
        /// </summary>
        public string ConsumerSecret { get; set; }

        /// <summary>
        /// Gets or sets the url target.
        /// </summary>
        public string UrlTarget { get; set; }

        /// <summary>
        /// Gets or sets the xpath expression.
        /// </summary>
        public string XpathExpression { get; set; }

        /// <summary>
        /// Gets or sets the file error folder.
        /// </summary>
        public string FileErrorFolder { get; set; }
        
        /// <summary>
        /// The post message.
        /// </summary>
        public void PostMessage()
        {
            foreach (string file in Files)
            {
                PushFileReader pushFileReader = new PushFileReader { Filename = file, XpathExpression = XpathExpression };
                pushFileReader.Read();
                string messageTitle = pushFileReader.GetTitle();
                string messagePublisher = pushFileReader.GetPublisher();
                
                string messageGetMessageId = pushFileReader.GetMessageId();

                if (UrlTarget != string.Empty)
                {
                    TwitterMessage twitterMessage = new TwitterMessage
                                                        {
                                                            Message = messageTitle,
                                                            Publisher = messagePublisher,
                                                            Url = UrlTarget + messageGetMessageId,
                                                            MessageLength = MessageLength
                                                        };
                    try
                    {
                        TwitterPushMessage twitterPushMessage = new TwitterPushMessage
                                                                    {
                                                                        MessageLength = this.MessageLength,
                                                                        SetMessage = twitterMessage,
                                                                        OAuthToken = AccessToken,
                                                                        AccessTokenSecret =
                                                                            AccessTokenSecret,
                                                                        ConsumerSecret = ConsumerSecret,
                                                                        ConsumerKey = ConsumerKey,
                                                                        AccessTokenUrl = AccessTokenUrl
                                                                    };
                        twitterPushMessage.Authorize();

                        twitterPushMessage.Tweet();

                        // Moving to done folder
                        Logger.Info("Moving file to done folder");

                        FileInfo fileInfo = new FileInfo(file);
                        string filename = fileInfo.Name;

                        string moveFilename = FileDoneFolder + @"\" + filename;
                        FileInfo fileInfoMove = new FileInfo(moveFilename);

                        if (fileInfoMove.Exists)
                        {
                            fileInfoMove.Delete();
                        }

                        fileInfo.MoveTo(moveFilename);
                    }
                    catch (Exception exception)
                    {
                        Logger.Error(exception.Message);
                        Logger.Error(exception.StackTrace);
                        Logger.Info("File is being moved to " + FileErrorFolder);

                        FileInfo fileInfo = new FileInfo(file);
                        string filename = fileInfo.Name;

                        string moveFilename = FileErrorFolder + @"\" + filename;
                        FileInfo fileInfoMove = new FileInfo(moveFilename);

                        if (fileInfoMove.Exists)
                        {
                            fileInfoMove.Delete();
                        }

                        fileInfo.MoveTo(moveFilename);
                    }
                }
                else
                {
                    // I am just here so I can stop the code
                    Logger.Info("No url!");
                }
            }
        }
    }
}