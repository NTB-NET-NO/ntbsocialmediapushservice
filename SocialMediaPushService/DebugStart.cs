﻿// -----------------------------------------------------------------------
// <copyright file="DebugStart.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace NTB.SocialMedia.Service
{
    using System;
    using System.Windows.Forms;

    /// <summary>
    /// The debug start.
    /// </summary>
    internal static class DebugStart
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new DebugForm());
        }
    }
}
