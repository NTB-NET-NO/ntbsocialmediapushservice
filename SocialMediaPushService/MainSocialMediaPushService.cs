﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainSocialMediaPushService.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SocialMedia.Service
{
    using System;
    using System.ComponentModel;
    using System.ServiceProcess;

    // Adding Log4Net - logging support
    using log4net;

    // Adding reference to components
    using NTB.SocialMedia.Components;

    /// <summary>
    /// The main social media push service.
    /// </summary>
    public partial class MainSocialMediaPushService : ServiceBase
    {
        /// <summary>
        /// The actual service object
        /// </summary>
        protected MainServiceComponent Service = new MainServiceComponent();

        /// <summary>
        /// The logger.
        /// </summary>
        private static ILog Logger = LogManager.GetLogger(typeof(MainSocialMediaPushService));

        /// <summary>
        /// Initializes a new instance of the <see cref="MainSocialMediaPushService"/> class. 
        /// The service.
        /// </summary>
        /// <summary>
        /// Initializes a new instance of the <see cref="MainSocialMediaPushService"/> class.
        /// </summary>
        public MainSocialMediaPushService()
        {
            log4net.Config.XmlConfigurator.Configure();
            MDC.Set("JOBNAME", "SocialMediaPushService");
            
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainSocialMediaPushService"/> class.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public MainSocialMediaPushService(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        /// <summary>
        /// When implemented in a derived class, executes when a Start command is sent to the service by the Service Control Manager (SCM) or when the operating system starts (for a service that starts automatically). Specifies actions to take when the service starts.
        /// </summary>
        /// <param name="args">Data passed by the start command.</param>
        protected override void OnStart(string[] args)
        {
            try
            {
                Logger.Info("SocialMediaPushService starting");
                Service.Configure();

                Logger.Info("SocialMediaPushService started");
            }
            catch (Exception ex)
            {
                Logger.Fatal("SocialMediaPushService DID NOT START properly", ex);
            }
        }

        /// <summary>
        /// When implemented in a derived class, executes when a Stop command is sent to the service by the Service Control Manager (SCM). Specifies actions to take when a service stops running.
        /// </summary>
        protected override void OnStop()
        {
            try
            {
                Service.Stop();
                Logger.Info("SocialMediaPushService stopped");
            }
            catch (Exception ex)
            {
                Logger.Fatal("SocialMediaPushService DID NOT STOP properly", ex);
            }
        }
    }
}