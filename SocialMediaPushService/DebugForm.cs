﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DebugForm.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------



namespace NTB.SocialMedia.Service
{
    using System;
    using System.Windows.Forms;

    // Adding support for log4net
    using log4net;

    // Adding reference to components
    using NTB.SocialMedia.Components;

    /// <summary>
    /// The debug form.
    /// </summary>
    public partial class DebugForm : Form
    {
        /// <summary>
        /// The actual service object
        /// </summary>
        /// <remarks>Does the actuall work</remarks>
        protected MainServiceComponent Service = null;

        /// <summary>
        /// Logger used to log events in the form
        /// </summary>
        private static ILog logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="DebugForm"/> class.
        /// </summary>
        public DebugForm()
        {
            ThreadContext.Properties["JOBNAME"] = "SocialMediaPushService-Debug";
            log4net.Config.XmlConfigurator.Configure();
            logger = LogManager.GetLogger(typeof(DebugForm));

            Service = new MainServiceComponent();

            InitializeComponent();
        }

        /// <summary>
        /// The debug form_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void DebugForm_Load(object sender, EventArgs e)
        {
            try
            {
                logger.Info("SocialMediaPushService DEBUGMODE starting...");
                _startupConfigTimer.Start();
            }
            catch (Exception ex)
            {
                logger.Fatal("SocialMediaPushService DEBUGMODE DID NOT START properly", ex);
            }
        }

        /// <summary>
        /// The debug form_ form closing.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void DebugForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                Service.Stop();
                logger.Info("SocialMediaPushService DEBUGMODE stopped");
            }
            catch (Exception ex)
            {
                logger.Fatal("SocialMediaPushService DEBUGMODE DID NOT STOP properly", ex);
            }
        }

        /// <summary>
        /// The _startup config timer_ tick.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void _startupConfigTimer_Tick(object sender, EventArgs e)
        {
            // Kill the timer
            _startupConfigTimer.Stop();

            // And configure
            try
            {
                Service.Configure();
                Service.Start();
                logger.Info("SocialMediaPushService DEBUGMODE started");
            }
            catch (Exception ex)
            {
                logger.Fatal("SocialMediaPushService DEBUGMODE DID NOT START properly", ex);
            }
        }
    }
}