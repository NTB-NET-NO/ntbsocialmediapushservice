﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceStart.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The service start.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SocialMedia.Service
{
    using System.ServiceProcess;

    // Adding support for log4Net logging
    using log4net;
    using log4net.Config;

    /// <summary>
    /// The service start.
    /// </summary>
    internal static class ServiceStart
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ServiceStart));

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static void Main()
        {
            XmlConfigurator.Configure();
            MDC.Set("JOBNAME", "NTB.SocialMediaPush.Service");
            Logger.Info("---------------------------------------------------------");
            Logger.Info("---    Starting up the NTB SocialMediaPush Service    ---");
            Logger.Info("---------------------------------------------------------");

            ServiceBase[] servicesToRun = new ServiceBase[]
                                              {
                                                  new MainSocialMediaPushService()
                                              };
            ServiceBase.Run(servicesToRun);
        }
    }
}